﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Electrician
{
    public class ComputingEquipment : AbstractElectricalAppliance
    {
        private string _calculatingSpeed;

        public string CalculatingSpeed
        {
            get { return _calculatingSpeed; }
            set { _calculatingSpeed = value; }
        }
    }
}