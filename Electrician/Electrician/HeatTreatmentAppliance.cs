﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Electrician
{
    public class HeatTreatmentAppliance : KitchenAppliances
    {
        private int _temperatureLimit;

        public int TemperatureLimit
        {
            get { return _temperatureLimit; }
            set { _temperatureLimit = value; }
        }
    }
}
