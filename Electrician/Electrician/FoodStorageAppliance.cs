﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Electrician
{
    public class FoodStorageAppliance : KitchenAppliances
    {
        private int _volume;

        public int Volume
        {
            get { return _volume; }
            set { _volume = value; }
        }
    }
}