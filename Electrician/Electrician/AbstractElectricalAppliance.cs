﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Electrician
{
    public class AbstractElectricalAppliance
    {
        private string _name;
        private int _power;
        private bool _plug;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private int Power
        {
            get { return _power; }
            set { _power = value; }
        }
        private bool Plug
        {
            get { return _plug; }
            set { _plug = value; }
        }
    }
}
