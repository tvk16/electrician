﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Electrician
{
    public class Flat
    {
        private List<AbstractElectricalAppliance> _appliances;

        public AbstractElectricalAppliance GetPower(AbstractElectricalAppliance power)
        {
            //
            return power;
        }
        public void AddDevice(AbstractElectricalAppliance appliance)
        {
            _appliances.Add(appliance);
        }
        public void DeleteDevice(AbstractElectricalAppliance appliance)
        {
            _appliances.Remove(appliance);
        }
        public AbstractElectricalAppliance ChangePlug(AbstractElectricalAppliance appliance)
        {
            //
            return appliance;
        }
    }
}
