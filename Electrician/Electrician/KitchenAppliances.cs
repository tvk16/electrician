﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Electrician
{
    public class KitchenAppliances : AbstractElectricalAppliance
    {
        private string _material;

        public string Material
        {
            get { return _material; }
            set { _material = value; }
        }
    }
}